﻿#include "pch.h"
#include "BoardAndHand.h"
#include "Deck.h"
#include "CardCombination.h"
#include "PokerScore.h"


class PokerCalculation
{
	PokerScore pokerScore;

public:

	PokerScore getPokerScore() const;

	void allCaseForRange(BoardAndHand boardAndHand, Deck deck, int k, int* results);

	void allBoardCase(BoardAndHand boardAndHand, Deck deck, int* result);

	void allTurnAndRiverCase(BoardAndHand boardAndHand, Deck deck, int* result);

	void allRiverCase(BoardAndHand boardAndHand, Deck deck, int* result);
};