#include "pch.h"
#include "Deck.h"

Deck::Deck()
{
	for(int suit = CLUB; suit != END_SUIT; suit++)
	{
		for(int rank = ACE; rank != END_RANK; rank++)
		{
			cards.push_back(Card((SUIT)suit, (RANK)rank));
		}
	}
}

Deck::~Deck()
{
}

vector<Card> Deck::getCards() const
{
	return cards;
}

void Deck::pop_back()
{
	cards.pop_back();
}

void Deck::setCard(int index, Card card)
{
	cards[index] = card;
}
