#include "pch.h"
#include "CardReader.h"

vector<Card> CardReader::readFile(FILE* file) {
	vector<Card> cardList;
	char c[2];
	c[0] = getc(file);
	while (c[0] != EOF)
	{
		c[1] = getc(file);
		if (c[1] == ' ')
			c[1] = getc(file);
		cardList.push_back(convertFromCharsToCard(c));
		c[0] = getc(file);
		if (c[0] == ' ')
			c[0] = getc(file);
	}
	if (cardList.size() != 2 && cardList.size() != 5 && cardList.size() != 6 && cardList.size() != 7)
	{
		throw "Wrong number of cards";
	}
	return cardList;
}

Card CardReader::convertFromCharsToCard(char card[]) {
	char rankChar = toupper(card[0]);
	char suitChar = tolower(card[1]);
	SUIT suit;
	RANK rank;
	switch (suitChar) {
	case 's':
		suit = SPADES;
		break;
	case 'c':
		suit = CLUB;
		break;
	case 'd':
		suit = DIAMOND;
		break;
	case 'h':
		suit = HEART;
		break;
	default:
		throw "Wrong suit value";
	}

	switch (rankChar) {
	case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
		rank = static_cast<RANK>(rankChar - '0');
		break;
	case 'T':
		rank = TEN;
		break;
	case 'J':
		rank = JACK;
		break;
	case 'Q':
		rank = QUEEN;
		break;
	case 'K':
		rank = KING;
		break;
	case 'A':
		rank = ACE;
		break;
	default:
		throw "Wrong rank value";
	}
	return Card(suit, rank);
}