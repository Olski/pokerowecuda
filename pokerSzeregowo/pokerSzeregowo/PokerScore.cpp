#include "pch.h"
#include "PokerScore.h"

int PokerScore::treeCheckScore(vector<Card> cards) {
	if (PokerLogic::isPair(cards) == ONE_PAIR)
		if (PokerLogic::isThreeOfAKind(cards) == THREE_OF_A_KIND)
			if (PokerLogic::isFourOfAKind(cards) == FOUR_OF_A_KIND)
				return 7;
			else if (PokerLogic::isFullHouse(cards) == FULL_HOUSE)
				return 6;
			else
				return treeCheckScoreStraightPart(cards, 3);
		else if (PokerLogic::isTwoPair(cards) == TWO_PAIR)
			return treeCheckScoreStraightPart(cards, 2);
		else
			return treeCheckScoreStraightPart(cards, 1);
	else
		return treeCheckScoreStraightPart(cards, 0);
}

int PokerScore::score(vector<Card> cards) {
	if (PokerLogic::isRoyalFlush(cards) == ROYAL_FLUSH)
		return 9;
	if (PokerLogic::isStraightFlush(cards) == STRAIGHT_FLUSH)
		return 8;
	if (PokerLogic::isFourOfAKind(cards) == FOUR_OF_A_KIND)
		return 7;
	if (PokerLogic::isFullHouse(cards) == FULL_HOUSE)
		return 6;
	if (PokerLogic::isFlush(cards) == FLUSH)
		return 5;
	if (PokerLogic::isStraight(cards) == STRAIGHT)
		return 4;
	if (PokerLogic::isThreeOfAKind(cards) == THREE_OF_A_KIND)
		return 3;
	if (PokerLogic::isTwoPair(cards) == TWO_PAIR)
		return 2;
	if (PokerLogic::isPair(cards) == ONE_PAIR)
		return 1;
	else
		return 0;
}

int PokerScore::treeCheckScoreStraightPart(vector<Card> cards, int theWeakestHand) {
	if (PokerLogic::isStraight(cards) == STRAIGHT)
		if (PokerLogic::isFlush(cards) == FLUSH)
			if (PokerLogic::isRoyalFlush(cards) == ROYAL_FLUSH)
				return 9;
			else if (PokerLogic::isStraightFlush(cards) == STRAIGHT_FLUSH)
				return 8;
			else
				return 5;
		else
			return 4;

	else if (PokerLogic::isFlush(cards) == FLUSH)
		return 5;
	else
		return theWeakestHand;
}