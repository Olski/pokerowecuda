#pragma once
#include "PokerCalculation.h"
using namespace std;

class PokerStatistic {

    static PokerCalculation pokerCalculation;

public:

	static PokerCalculation getpokerCalculation();

	static string printResult(int* results, vector<Card> cardList);

	static int countIterations(vector<Card> cardList);

	static void count(vector<Card> entryCards, int* result);

	static double round(double val);
};