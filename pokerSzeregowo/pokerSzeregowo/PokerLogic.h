#pragma once
#include "Card.h"
#include <vector>
using namespace std;

class PokerLogic {

public:

	static POKER_RANK isRoyalFlush(vector<Card> cards);

	static POKER_RANK isStraightFlush(vector<Card> cards);

	static POKER_RANK isFourOfAKind(vector<Card> cards);

	static POKER_RANK isFullHouse(vector<Card> cards);

	static POKER_RANK isFlush(vector<Card> cards);

	static POKER_RANK isStraight(vector<Card> cards);

	static POKER_RANK isThreeOfAKind(vector<Card> cards);

	static POKER_RANK isTwoPair(vector<Card> cards);

	static POKER_RANK isPair(vector<Card> cards);

private:
    
	static POKER_RANK isBrodwayStraight(vector<Card> cardList);

	static SUIT suitInFlush(vector<Card> cards);

	static void deleteSuitsOtherThan(vector<Card> hand, SUIT suit);
};
