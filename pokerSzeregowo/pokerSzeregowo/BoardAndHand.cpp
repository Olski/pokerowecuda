#include "pch.h"
#include "BoardAndHand.h"

using namespace  std;
BoardAndHand::BoardAndHand()
{
}


BoardAndHand::~BoardAndHand()
{
}

vector<Card> BoardAndHand::getCards() const
{
	return cards;
}

void BoardAndHand::setCards(const vector<Card>& cards)
{
	this->cards = cards;
}

void BoardAndHand::addCard(Card card)
{
	cards.push_back(card);
}

void BoardAndHand::setCard(int index, Card card)
{
	if(cards.size() >= index)
		cards.push_back(card);
	else
		cards[index] = card;
}

void BoardAndHand::setAllCard(int indexFrom, vector<Card> cardList)
{
	for(Card card : cardList)
	{
		setCard(indexFrom++, card);
	}
}

void BoardAndHand::sort()
{
	//std::sort(this->getCards().begin(), this->getCards().end(), greater<Card>());
	QuickSort(0, cards.size() - 1);
}

void BoardAndHand::QuickSort(int left, int right)
{
	int middle = cards[(left + right) / 2].getRank();
	Card temp;
	int i = left;
	int j = right;
	do {
		while (cards[i].getRank() < middle) i++;
		while (cards[j].getRank() > middle) j--;
		if (i <= j) {
			temp = cards[i];
			cards[i] = cards[j];
			cards[j] = temp;
			i++; j--;
		}
	} while (i <= j);
	if (j > left) QuickSort(left, j);
	if (i < right) QuickSort(i, right);
}
