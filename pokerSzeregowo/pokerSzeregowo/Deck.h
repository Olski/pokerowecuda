#pragma once
#include "Card.h"
#include <vector>
using namespace  std;
class Deck
{
public:

	vector<Card> getCards() const;

	void pop_back();

	void setCard(int index, Card card);

	Deck();

	~Deck();

private:
	vector<Card> cards;
};

