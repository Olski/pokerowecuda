#include "pch.h"
#include <cstdio>
#include <ctime>
#include <vector>
#include "CardReader.h"
#include "PokerStatistic.h"
#include  <iostream>

int main()
{
	clock_t start;
	clock_t stop;

	FILE* file = fopen("test.txt", "r+");
	vector<Card> list;
	list = CardReader::readFile(file);
	start = clock();
	int* result = (int*)calloc(10, sizeof(int));
	PokerStatistic::count(list, result);
	stop = clock();
	printf("Czas: %lums\n", stop - start);
	cout<< PokerStatistic::printResult(result, list);

	getchar();
	return 0;
}
