#include "pch.h"
#include "CardCombination.h"

BigInteger CardCombination::factorialTab[] = {
				BigInteger("1"),
				BigInteger("1"),
				BigInteger("2"),
				BigInteger("6"),
				BigInteger("24"),
				BigInteger("120"),
				BigInteger("720"),
				BigInteger("5040"),
				BigInteger("40320"),
				BigInteger("362880"),
				BigInteger("3628800"),
				BigInteger("39916800"),
				BigInteger("479001600"),
				BigInteger("6227020800"),
				BigInteger("87178291200"),
				BigInteger("1307674368000"),
				BigInteger("20922789888000"),
				BigInteger("355687428096000"),
				BigInteger("6402373705728000"),
				BigInteger("121645100408832000"),
				BigInteger("2432902008176640000"),
				BigInteger("51090942171709440000"),
				BigInteger("1124000727777607680000"),
				BigInteger("25852016738884976640000"),
				BigInteger("620448401733239439360000"),
				BigInteger("15511210043330985984000000"),
				BigInteger("403291461126605635584000000"),
				BigInteger("10888869450418352160768000000"),
				BigInteger("304888344611713860501504000000"),
				BigInteger("8841761993739701954543616000000"),
				BigInteger("265252859812191058636308480000000"),
				BigInteger("8222838654177922817725562880000000"),
				BigInteger("263130836933693530167218012160000000"),
				BigInteger("8683317618811886495518194401280000000"),
				BigInteger("295232799039604140847618609643520000000"),
				BigInteger("10333147966386144929666651337523200000000"),
				BigInteger("371993326789901217467999448150835200000000"),
				BigInteger("13763753091226345046315979581580902400000000"),
				BigInteger("523022617466601111760007224100074291200000000"),
				BigInteger("20397882081197443358640281739902897356800000000"),
				BigInteger("815915283247897734345611269596115894272000000000"),
				BigInteger("33452526613163807108170062053440751665152000000000"),
				BigInteger("1405006117752879898543142606244511569936384000000000"),
				BigInteger("60415263063373835637355132068513997507264512000000000"),
				BigInteger("2658271574788448768043625811014615890319638528000000000"),
				BigInteger("119622220865480194561963161495657715064383733760000000000"),
				BigInteger("5502622159812088949850305428800254892961651752960000000000"),
				BigInteger("258623241511168180642964355153611979969197632389120000000000"),
				BigInteger("12413915592536072670862289047373375038521486354677760000000000"),
				BigInteger("608281864034267560872252163321295376887552831379210240000000000"),
				BigInteger("30414093201713378043612608166064768844377641568960512000000000000")
		};

int CardCombination::newtonSymbol(int n, int k) {
	if (n < 0 || k < 0 || k > n)
		return 0;
	return (factorialTab[n] / (factorialTab[k] * (factorialTab[n - k]))).longValue();
}

vector<Card> CardCombination::cardCombinationByNumber(vector<Card> deck, int n, int k, int numberCombination) {
	return fillListByCards(deck, combination(numberCombination, n, k), k);
}

vector<Card> CardCombination::fillListByCards(vector<Card> deck, int* array, int k) {
	vector<Card> cardList;
	for (int i = 0; i < k; i++) {
		cardList.push_back(deck[array[i]]);
	}
	return cardList;
}

int* CardCombination::combination(int number_comb, int n, int k) {
	int* result = new int[k];

	for (int i = 0, iter = 0; iter < k && i < n; i++) {
		int ns = newtonSymbol(n - i - 1, k - iter - 1);
		if (number_comb < ns) {
			result[iter++] = i;
		}
		else {
			number_comb -= ns;
		}
	}
	return result;
}