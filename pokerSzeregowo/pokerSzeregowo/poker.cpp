﻿#include "pch.h"
#include <cstdio>
#include <ctime>
#include <vector>
#include "Card.h"
#include "CardReader.h"
#include "PokerStatistic.h"

int main()
{
	clock_t start;
	clock_t stop; 

	FILE* file = fopen("test.txt", "r+");
	vector<Card> list;
	list = CardReader::readFile(file);
	start = clock();
	int* result = PokerStatistic::count(list);
	stop = clock();
	printf("Czas: %lu\n", stop - start);
	printf("%s\n", PokerStatistic::printResult(result, list));

	getchar();
}
