﻿#include "pch.h"
#include "PokerCalculation.h"

PokerScore pokerScore = PokerScore();

PokerScore PokerCalculation::getPokerScore() const
{
	return pokerScore;
}

void PokerCalculation::allCaseForRange(BoardAndHand boardAndHand, Deck deck, int k, int* results) {
	BoardAndHand boardAndHandCopy;
	int n = 52 - 7 + k;
	int index = 7 - k;
	for (int i = 0; i < CardCombination::newtonSymbol(n, k); i++) {
		boardAndHandCopy = boardAndHand;
		boardAndHandCopy.setAllCard(index, CardCombination::cardCombinationByNumber((deck.getCards()), n, k, i));
		boardAndHandCopy.sort();
		int pokerRankInt = pokerScore.treeCheckScore(boardAndHandCopy.getCards());
		results[pokerRankInt] = results[pokerRankInt] + 1;
	}
}

void PokerCalculation::allBoardCase(BoardAndHand boardAndHand, Deck deck, int* result) {
	allCaseForRange(boardAndHand, deck, 5, result);
}

void PokerCalculation::allTurnAndRiverCase(BoardAndHand boardAndHand, Deck deck, int* result) {
	allCaseForRange(boardAndHand, deck, 2, result);
}

void PokerCalculation::allRiverCase(BoardAndHand boardAndHand, Deck deck, int* result) {
	allCaseForRange(boardAndHand, deck, 1, result);
}