#pragma once
#include <vector>
#include <stdio.h>
#include "Card.h"
using namespace std;

class CardReader {

public:

	static vector<Card> readFile(FILE* file);

private:

	static Card convertFromCharsToCard(char card[]);
};
