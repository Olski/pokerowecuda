#include "pch.h"
#include "PokerLogic.h"

POKER_RANK PokerLogic::isRoyalFlush(vector<Card> cards) {
	int counter = 0;
	SUIT suit = suitInFlush(cards);
	if (suit == EMPTY_SUIT)
		return HIGH_CARD;
	for (Card card : cards) {
		if (card.getRank() == TEN && card.getSuit() == suit)
			counter++;
		if (card.getRank() == JACK && card.getSuit() == suit)
			counter++;
		if (card.getRank() == QUEEN && card.getSuit() == suit)
			counter++;
		if (card.getRank() == KING && card.getSuit() == suit)
			counter++;
		if (card.getRank() == ACE && card.getSuit() == suit)
			counter++;
	}
	if (counter == 5)
		return ROYAL_FLUSH;

	return HIGH_CARD;
}

POKER_RANK PokerLogic::isStraightFlush(vector<Card> cards) {
	SUIT suit = suitInFlush(cards);
	if (suit == EMPTY_SUIT)
		return HIGH_CARD;
	vector<Card> cardList = cards;
	deleteSuitsOtherThan(cardList, suit);

	if (isBrodwayStraight(cardList) == STRAIGHT)
		return STRAIGHT_FLUSH;
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isFourOfAKind(vector<Card> cards) {
	for (int iter = 0; iter < cards.size() - 3; iter++) {
		if ((cards[iter].compareRank(cards[1 + iter]) == 0)
			&& (cards[1 + iter].compareRank(cards[2 + iter]) == 0)
			&& (cards[2 + iter].compareRank(cards[3 + iter]) == 0))
			return FOUR_OF_A_KIND;
	}
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isFullHouse(vector<Card> cards) {
	for (int iter = 0; iter < cards.size() - 2; iter++)
		if (cards[iter].compareRank(cards[1 + iter]) == 0
			&& cards[1 + iter].compareRank(cards[2 + iter]) == 0)
			for (int i = 0; i < cards.size() - 1; i++)
				if (iter > i + 1 || iter + 2 < i)
					if (cards[i].compareRank(cards[i + 1]) == 0)
						return FULL_HOUSE;
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isFlush(vector<Card> cards) {
	if (suitInFlush(cards) != EMPTY_SUIT)
		return FLUSH;
	else
		return HIGH_CARD;
}

POKER_RANK PokerLogic::isStraight(vector<Card> cards) {
	vector<Card> cardList = cards;
	for (Card c : cards) {
		if (c.getRank() == ACE)
			cardList.push_back(c);
	}
	Card card = cardList[0];
	int counter = 1;
	for (int i = 1; i < cardList.size(); i++) {
		if (card.getRankInt() == cardList[i].getRankInt()) {
			card = cardList[i];
			continue;
		}
		else if (card.getRankInt() + 1 == cardList[i].getRankInt()
			|| (card.getRank() == KING && cardList[i].getRank() == ACE)) {
			counter++;
			if (counter >= 5)
				return STRAIGHT;
		}
		else {
			counter = 1;
			if (i > 3)
				return HIGH_CARD;
		}
		card = cardList[i];
	}
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isThreeOfAKind(vector<Card> cards) {
	for (int x = 0; x < cards.size() - 2; x++)
		if (cards[x].compareRank(cards[1 + x]) == 0
			&& cards[1 + x].compareRank(cards[2 + x]) == 0)
			return THREE_OF_A_KIND;
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isTwoPair(vector<Card> cards) {
	for (int x = 0; x < cards.size() - 1; x++)
		if (cards[x].compareRank(cards[1 + x]) == 0)
			for (int i = 0; i < cards.size() - 1; i++)
				if (x > i + 1 || x + 1 < i)
					if (cards[i].compareRank(cards[i + 1]) == 0)
						return TWO_PAIR;
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isPair(vector<Card> cards) {
	for (int x = 0; x < cards.size() - 1; x++)
		if ((cards[x].compareRank(cards[1 + x])) == 0)
			return ONE_PAIR;
	return HIGH_CARD;
}

POKER_RANK PokerLogic::isBrodwayStraight(vector<Card> cardList) {
	Card card = cardList[0];
	int counter = 1;
	for (int i = 1; i < cardList.size(); i++) {
		if (card.getRankInt() == cardList[i].getRankInt()) {
			card = cardList[i];
			continue;
		}
		else if (card.getRankInt() + 1 == cardList[i].getRankInt()) {
			counter++;
			if (counter >= 5)
				return STRAIGHT;
		}
		else {
			counter = 1;
			if (i > 2)
				return HIGH_CARD;
		}
		card = cardList[i];
	}
	return HIGH_CARD;
}

SUIT PokerLogic::suitInFlush(vector<Card> cards) {
	int diamonds = 0;
	int hearts = 0;
	int clubs = 0;
	int spades = 0;

	for (Card x : cards) {
		if (x.getSuit() == DIAMOND)
			diamonds++;
		else if (x.getSuit() == HEART)
			hearts++;
		else if (x.getSuit() == CLUB)
			clubs++;
		else if (x.getSuit() == SPADES)
			spades++;
	}
	if (diamonds > 4)
		return DIAMOND;
	else if (hearts > 4)
		return HEART;
	else if (clubs > 4)
		return CLUB;
	else if (spades > 4)
		return SPADES;
	return EMPTY_SUIT;
}

void PokerLogic::deleteSuitsOtherThan(vector<Card> hand, SUIT suit) {
	for (int i = 0; i < hand.size(); i++) {
		if (hand[i].getRank() != suit)
			hand.erase(hand.begin() + i);
	}
}