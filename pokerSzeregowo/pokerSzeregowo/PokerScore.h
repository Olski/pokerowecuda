#pragma once
#include "PokerLogic.h"

class PokerScore 
{

public:

	int treeCheckScore(vector<Card> cards);

	int score(vector<Card> cards);

	int treeCheckScoreStraightPart(vector<Card> cards, int theWeakestHand);
};
