#include "pch.h"
#include "PokerStatistic.h"


PokerCalculation PokerStatistic::pokerCalculation = PokerCalculation::PokerCalculation();

PokerCalculation PokerStatistic::getpokerCalculation() {
	return pokerCalculation;
}

string PokerStatistic::printResult(int* results, vector<Card> cardList) {
	int iterations = countIterations(cardList);
	double res[10];
	for (int i = 0; i < 10; i++) {
		res[i] = round(results[i] / (double)iterations * 100.0 * 1000.0) / 1000.0;
	}

	stringstream ss;
	ss << "===========================\n" <<
		"High Card: " << res[0] << "%\n" <<
		"Pair: " << res[1] << "%\n" <<
		"Two Pair: " << res[2] << "%\n" <<
		"Three of Kind: " << res[3] << "%\n" <<
		"Straight: " << res[4] << "%\n" <<
		"Flush: " << res[5] << "%\n" <<
		"Full House: " << res[6] << "%\n" <<
		"Four of Kind: " << res[7] << "%\n" <<
		"Straight Flush: " << res[8] << "%\n" <<
		"Royal Flush: " << res[9] << "%\n" <<
		"===========================\n\n\n";
	return ss.str();
}

int PokerStatistic::countIterations(vector<Card> cardList) {
	switch (cardList.size()) {
	case 2:
		return CardCombination::newtonSymbol(50, 5);
	case 5:
		return CardCombination::newtonSymbol(47, 2);
	case 6:
		return  46; // (newtonSymbol(46,1));
	case 7:
		return  1; // newtonSymbol(45,0));
	}
	return 0;
}

void PokerStatistic::count(vector<Card> entryCards, int* result) {
	Deck deck = Deck();
	BoardAndHand boardAndHand = BoardAndHand();

	for (int i = 0; i < entryCards.size(); i++) {
		boardAndHand.addCard(entryCards[i]);
		for (int j = 0; j < deck.getCards().size(); j++)
		{
			if (entryCards[i] == deck.getCards()[j]) {
				if (j != deck.getCards().size() - 1)
					deck.setCard(j, deck.getCards().back());
				deck.pop_back();
			}
		}
	}
	boardAndHand.sort();
	switch (entryCards.size()) {
	case 2:
		return pokerCalculation.allBoardCase(boardAndHand, deck, result);
	case 5:
		return pokerCalculation.allTurnAndRiverCase(boardAndHand, deck, result);
	case 6:
		return pokerCalculation.allRiverCase(boardAndHand, deck, result);
	case 7:
		result[pokerCalculation.getPokerScore().treeCheckScore(entryCards)] = 1;
	}
}

double PokerStatistic::round(double val)
{
	if (val < 0)
		return ceil(val - 0.5);
	return floor(val + 0.5);
}