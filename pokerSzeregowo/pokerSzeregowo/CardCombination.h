#include "pch.h"
#include "BigInteger.h"
#include "Card.h"

using namespace  std;

class CardCombination
{
	static BigInteger factorialTab[51];

public:

	static int newtonSymbol(int n, int k);

	static vector<Card> cardCombinationByNumber(vector<Card> deck, int n, int k, int numberCombination);

	static vector<Card> fillListByCards(vector<Card> deck, int* array, int k);

	static int* combination(int number_comb, int n, int k);
};
