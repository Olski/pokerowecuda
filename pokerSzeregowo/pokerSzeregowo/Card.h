#pragma once
#include <string>
#include <sstream>

enum RANK
{
	EMPTY, ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, END_RANK
};

enum SUIT {
	EMPTY_SUIT, CLUB, DIAMOND, HEART, SPADES, END_SUIT
};

enum POKER_RANK {
	HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND,
	STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH,
	ROYAL_FLUSH
};

class Card
{
public:

	SUIT suit;
	RANK rank;

	Card();

	~Card();

	Card(SUIT suit, RANK rank);

	SUIT getSuit() const;

	void setSuit(SUIT suit);

	RANK getRank() const;

	void setRank(RANK rank);

	int getRankInt();

	int compareRank(Card card);

	bool operator > (const Card& card) const;

	std::string to_string();

private:

	friend bool operator==(const Card& lhs, const Card& rhs)
	{
		return lhs.suit == rhs.suit
			&& lhs.rank == rhs.rank;
	}

	friend bool operator!=(const Card& lhs, const Card& rhs)
	{
		return !(lhs == rhs);
	}
};

