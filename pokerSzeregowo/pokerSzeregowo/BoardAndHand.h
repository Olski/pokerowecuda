#pragma once
#include "Card.h"
#include <vector>
using namespace std;

class BoardAndHand
{
public:

	BoardAndHand();

	~BoardAndHand();

	vector<Card> getCards() const;

	void setCards(const vector<Card>& cards);

	void addCard(Card card);

	void setCard(int index, Card card);

	void setAllCard(int indexFrom, vector<Card> cardList);

	void sort();

private:

	vector<Card> cards;

	void QuickSort(int left, int right);
};

