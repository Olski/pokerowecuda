#include "pch.h"
#include "Card.h"


Card::Card()
{
}


Card::~Card()
{
}


Card::Card(SUIT suit, RANK rank)
	: suit(suit),
	rank(rank)
{
}

SUIT Card::getSuit() const
{
	return suit;
}

void Card::setSuit(SUIT suit)
{
	this->suit = suit;
}

RANK Card::getRank() const
{
	return rank;
}

void Card::setRank(RANK rank)
{
	this->rank = rank;
}

int Card::getRankInt()
{
	return this->rank;
}

int Card::compareRank(Card card)
{
	if (this->rank > card.getRank())
		return 1;
	if (this->rank == card.getRank())
		return 0;
	return -1;
}

bool Card::operator > (const Card& card) const
{
	return (rank > card.getRank());
}

std::string Card::to_string()
{
	std::string SuitTypes[] =
	{
		"EMPTY_SUIT", "CLUB", "DIAMOND", "HEART", "SPADES"
	};
	std::stringstream ss;
	ss << this->getRank() <<
		SuitTypes[this->getSuit()];
	return ss.str();
}
